# tokyocabinet-tch

A chicken egg for tokyocabinet. Currently, it supports only tch files and only for reading.

## Functions for tch databases

(tc-hdb-open file #!key flags)

(tc-hdb-close hdb)

(tc-hdb-ecode hdb)

(tc-hdb-get hdb key)

(tc-hdb-get-exp db key)
reads from the string returned by tc-hdb-get

